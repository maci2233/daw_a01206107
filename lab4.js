function fun1(){
	let num = prompt("Ingresa un número");
	for(var i=1 ; i<=num ; i++){
		document.write(i + "\t" + i*i + "\t" + i*i*i);
	}
}

function fun2(){
	var num1 = Math.floor((Math.random()*100)+1);
	var num2 = Math.floor((Math.random()*100)+1);
	var res = prompt(num1 + " + " +num2 + " = ?");
	if(res == num1+num2){
		alert("Correcto");
	}
	else{
		alert("Resultado incorrecto");
	}
}
function fun3(){
	var n = prompt("Ingresa el tamaño del arreglo");
	var x;
	var arreglo = [];
	for(var i=0;i<n;i++){
		x = prompt("Ingresa el valor "+(i+1)+" del arreglo");
		arreglo[i]=x; 
	}
	var menor =0;
	var igual = 0;
	var mayor = 0;
	for(var i=0;i<n;i++){
		if(arreglo[i]<0){
			menor++;
		}
		else if(arreglo[i]>0){
			mayor++;
		}
		else{
			igual++;
		}
	}
	alert("El arreglo tiene "+menor+" numeros negativos, "+igual+" numeros iguales a 0 y "+mayor+" numeros mayores a 0");
}

function fun4(){
}

function fun5(){
	let pal = prompt("Ingresa el numero a invertir");
	let word = pal.length;
	let arreglo = [];
	for(var i=0;i<pal.length;i++){
		arreglo[i] = pal.charAt(i);
	}
	let aux;
	let n=arreglo.length;
	n--;
	for(var i=0;i<(n+1)/2;i++,n--){
		aux=arreglo[i];
		arreglo[i]=arreglo[n];
		arreglo[n]=aux;
	}
	alert("Resultado de la inversion =  "+arreglo);

}

function fun6(){
	var num = prompt("Ingresa un numero que se acomodara de menor a mayor");
	var arreglo = [];
	for(var i = 0;i<num.length;i++){
		arreglo[i] = num.charAt(i);
	}
	var n = num.length;
	for(var j=0;j<n;j++){
		for(var i=0;i<num.length-1;i++){
			if(arreglo[i]>arreglo[i+1]){
				let aux = arreglo[i];
				arreglo[i]=arreglo[i+1];
				arreglo[i+1]=aux;
			}
		}
	}
	alert("Arreglo ordenado" +arreglo);
}