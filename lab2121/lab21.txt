- �Qu� hace el primer bloque del c�digo (bloque del IF)? 

R: Si ya existe el procedimiento con el nombre "creaMaterial" lo elimina

- �Para qu� sirve la instrucci�n GO? 

R: Para realizar ciertas instrucciones dentro de un mismo procedimiento, por ejemplo, seg�n entend� no puedes definir una variable
   dentro de un bloque GO y despu�s usarla en otro GO porque se utiliz� de forma local.

- �Explica que recibe como par�metro este Procedimiento y qu� tabla modifica?

R: recibe la clave, la descripci�n, el costo y el impuesto y modifica la tabla Materiales agregando un nuevo registro con los valores ingresados.

  EXECUTE queryMaterial 'Lad',20 

- Explica en tu reporte qu� recibe como par�metro este Procedimiento y qu� hace

R: Recibe como par�metro la descripci�n del material y su costo. En este caso los parametros a pasar tienen los valores 'Lad' y 20. Lo que hace
   el procedimiento es realizar una consulta de los materiales que tienen "lad" en alguna parte de su descripci�n y que su costo es mayor a 20.

- �Qu� ventajas tienen el utilizar Store Procedures en una aplicaci�n cliente-servidor? 

R: Tiene bastantes, pero un ejemplo claro es la rapidez que ofrecen los stores procedures, ya que los datos se procesan en el servidor,
   y de esta forma lo que viaja a trav�s de la red son los resultados finales y no todos los datos.

- �Qu� ventajas tiene utilizar SP en un proyecto?

R: Es la forma m�s ideal de trabajar utilizando un modelo en capas, adem�s, como los stored procedures son independientes 
   de los lenguajes de programaci�n entonces cada persona puede programar puede programar en un lenguaje diferente y llamar a los mismos
   stored procedures sin problema.

/*STORE PROCEDURE PARA PROYECTO DE LA FUNDACI�N EL ARCA*/

/*AGREGANDO REGISTRO A LA TABLA VOLUNTARIO*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaVoluntario' AND type = 'P')
    DROP PROCEDURE creaVoluntario
GO

CREATE PROCEDURE creaVoluntario
    @unombre VARCHAR(50),
    @uapellidos VARCHAR(50),
    @ulugar VARCHAR(100),
    @ufecha DATETIME,
    @ucorreo VARCHAR(70),
    @utelefono VARCHAR(15),
    @uciudad VARCHAR(30),
    @upais VARCHAR(30),
    @uinteres NUMERIC(1)
AS
    INSERT INTO Voluntario VALUES(@unombre, @uapellidos,@ulugar,@ufecha,@ucorreo,@utelefono,@uciudad,@upais,@uinteres)
GO