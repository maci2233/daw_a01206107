<?php

	include 'header.html';

	function conecta (){
		$mysql = mysqli_connect("localhost","root","","talleres");
 		$mysql->set_charset("utf8");
 		return $mysql;
	}

	function desconecta($mysql){
		mysqli_close($mysql);
	}

	function selectVoluntarios(){
		$db = conecta();
		$query = 'SELECT * FROM voluntario';
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function selectITESM(){
		$db = conecta();
		$query = 'SELECT * FROM voluntario WHERE escuela = "ITESM"';
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function selectErick(){
		$db = conecta();
		$query = 'SELECT * FROM voluntario WHERE nombre LIKE "%Erick%"';
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function showVoluntarios(){
		$result = selectVoluntarios();
		echo "<br><br><h4>Lista de voluntarios</h4><br><br>";
		echo "<table class=bordered>";
		echo "<tr>";
		echo "<th>Nombre</th>";
		echo "<th>Telefono</th>";
		echo "<th>Matricula</th>";
		echo "<th>Escuela</th>";
		echo "</tr>";
		while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
			echo "<tr>";
			echo "<td>" . $fila["nombre"] . "</td>";
			echo "<td>" . $fila["telefono"] . "</td>";
			echo "<td>" . $fila["matricula"] . "</td>";
			echo "<td>" . $fila["escuela"] . "</td>";
			echo "</tr>";
		}
		echo "</table>";
	}

	function showITESM(){
		$result = selectITESM();
		echo "<br><br><h4>Lista de voluntarios que estudian en el ITESM</h4><br><br>";
		echo "<table class=bordered>";
		echo "<tr>";
		echo "<th>Nombre</th>";
		echo "<th>Telefono</th>";
		echo "<th>Matricula</th>";
		echo "<th>Escuela</th>";
		echo "</tr>";
		while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
			echo "<tr>";
			echo "<td>" . $fila["nombre"] . "</td>";
			echo "<td>" . $fila["telefono"] . "</td>";
			echo "<td>" . $fila["matricula"] . "</td>";
			echo "<td>" . $fila["escuela"] . "</td>";
			echo "</tr>";
		}
		echo "</table>";

	}

	function showErick(){
		$result = selectErick();
		echo "<br><br><h4>Lista de voluntarios que se llaman Erick</h4>	<br><br>";
		echo "<table class=bordered>";
		echo "<tr>";
		echo "<th>Nombre</th>";
		echo "<th>Telefono</th>";
		echo "<th>Matricula</th>";
		echo "<th>Escuela</th>";
		echo "</tr>";
		while ($fila = mysqli_fetch_array($result,MYSQLI_BOTH)) {
			echo "<tr>";
			echo "<td>" . $fila["nombre"] . "</td>";
			echo "<td>" . $fila["telefono"] . "</td>";
			echo "<td>" . $fila["matricula"] . "</td>";
			echo "<td>" . $fila["escuela"] . "</td>";
			echo "</tr>";
		}
		echo "</table>";
	}

	showVoluntarios();
	echo "<br><br>";
	showITESM();
	echo "<br><br>";
	showErick();
	echo "<br><br><br>";
	echo "<h4>Cuestionario lab 14</h4>";
	echo "<ol>
          <li><strong>¿Qué es ODBC y para qué es útil?</strong><br><p>ODBC es un estándar de acceso a las bases de datos desarrollado por SQL Access Group. El objetivo de ODBC es hacer posible el acceder a cualquier dato desde cualquier aplicación, sin importar qué sistema de gestión de bases de datos (DBMS) almacene los datos.</p></li>
          <li><strong>¿Qué es SQL Injection?</strong><br><p>Se dice que existe o se produjo una inyección SQL cuando, de alguna manera, se inserta código SQL invasor dentro del código SQL programado, a fin de alterar el funcionamiento normal del programa y lograr así que se ejecute la porción de código invasor incrustado, en la base de datos.</p></li>
          <li><strong>¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection?</strong><br><p>En PHP se tiene la función mysql_real_escape_string que se introduce en la consulta</p></li>
        </ol>";

	include 'footer.html';
?>