<!DOCTYPE html>
<html>
<head>
	<title>Lab9</title>
</head>
<body>
<?php
	function promedio($nums){

		$prom = 0;
		$n = count($nums);
		for ($i=0;$i<$n;$i++) { 
			$prom = $prom + $nums[$i];
		}
		$prom = $prom / $n;
		return $prom;
	}

	function mediana($nums){
		sort($nums);
		$n = count($nums);
		if($n % 2 == 0){
			$med = ($nums[$n/2] + $nums[($n/2)-1])/2;
		}
		else{
			$med = $nums[floor($n/2)];
		}
		return $med;
	}

	function muestra($nums){
		for ($i=0; $i < count($nums)-1; $i++) { 
			echo $nums[$i] . " , ";
		}
		echo $nums[$i] . " ]<br>";
	}

	function imprime($nums){
		$prom = promedio($nums);
		$med = mediana($nums);
		echo "EL ARREGLO ORIGINAL ES: [ ";
		muestra($nums);
		sort($nums);
		echo "<ul><li>PROMEDIO = ". $prom ."</li> <li>MEDIANA = " . $med . "</li></ul>";
		echo "EL ARREGLO ORDENADO DE MENOR A MAYOR: [ ";
		muestra($nums);
		echo "<br>EL ARREGLO ORDENADO DE MAYOR A MENOR: [ ";
		rsort($nums);
		muestra($nums);
		echo "<br><br>";
	}

	function tabla($n){
		echo "<br>IMPRIMIENTO CUADRADOS Y CUBOS DESDE EL 1 HASTA EL ".$n;
		$tabla = "<br><table><tr><td>NUMERO</td><td>CUADRADO</td><td>CUBO</td></tr>";
		for($i=1; $i<= $n; $i++){ 
			$tabla.="<tr><td>". $i ."</td><td>". $i*$i ."</td><td>". $i*$i*$i ."</td></tr>";
		}	
		$tabla.="</table>";
		echo $tabla;
	}

	function multiplica($nums){
		echo "<br><hr><br>ARREGLO: [ ";
		muestra($nums);
		$total=1;
		$n = count($nums);
		for ($i=0; $i < $n; $i++) { 
			$total = $total * $nums[$i];
		}
		echo "La multiplicación de todos los valores = ".$total;
	}


	$arreglo1 = array(2,1,3,4,10,6,9,8,7,5);
	$arreglo2 = array(21,63,32,60,81,94,41,33,3,7,8,73,30);	

	imprime($arreglo1);
	imprime($arreglo2);
	tabla(8);
	tabla(15);
	multiplica($arreglo1);

?>
<hr>
<h3>PREGUNTAS LAB 9</h3>
<ul>
	<li><strong>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</strong><p>Muestra gran cantidad de información sobre el estado actual de PHP. Incluye información sobre las opciones de compilación y extensiones de PHP, versión de PHP, información del servidor y entorno (si se compiló como módulo), entorno PHP, versión del OS, rutas, valor de las opciones de configuración locales y generales, cabeceras HTTP y licencia de PHP.</p></li>
	<li><strong>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</strong><p>El servidor tiene que ser capaz de recibir consultas realizadas del lado del cliente, pero respetando la seguridad adecuada que debe tener. Es importante tener un control de los usuarios que quieren acceder, posiblemente el acceso sea siempre restringido o tal vez se le permita acceder a ciertos usuarios autenticaods.</p></li>
	<li><strong>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</strong><p>Todo el código que se realiza en PHP se ejecuta del lado del servidor aunque puedes modificar el HTML. El código PHP se ejecuta antes de que el servidor le mande algo al usuario</p></li>
</ul>
</body>
</html>