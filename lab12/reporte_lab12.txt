Revisa el contenido de la tabla materiales y determina si existe alguna inconsistencia en el contenido de la tabla. 

- �Cu�l es? 

R: S� existe inconsistencia, porque la clave de 'xxx' que es 1000, se repite en otra tupla, cuando en teor�a la clave debe ser �nica. 

- �A qu� se debe? 

R: Se debe a que Clave no est� definido como llave primaria, entonces SQL te permite repetir el valor de ese campo sin ning�n problema.

Agrega nuevamente el registro a la tabla de materiales: 
  INSERT INTO Materiales values(1000, 'xxx', 1000) 

- �Qu� ocurri�? 

R: Ya no permite que se agregue a la tabla porque se repite la clave con la de otro producto.

  sp_helpconstraint materiales 

- �Qu� informaci�n muestra esta consulta? 

MUESTRA LA LLAVE PRIMARIA Y SU INFORMACION

- �Qu� sentencias utilizaste para definir las llaves primarias? 

ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)

- �Qu� sentencias utilizaste para definir este constrait? 

ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave,RFC,Numero,Fecha)

- �Qu� particularidad observas en los valores para clave, rfc y numero? 

R: Que los valores de las dem�s tuplas siguen cierto patr�n de escritura, la clave empieza de 1000 y el n�mero de 5000 por ejemplo pero esta nueva tupla rompe con la consistencia que se ten�a

- �C�mo responde el sistema a la inserci�n de este registro? 

R: No hace nada raro, simplemente lo agrega a la tabla porque t�cnicamente cumple con los tipos de datos correspondientes a cada columna.

Intenta nuevamente la inserci�n del registro inconsistente. 

- �Qu� significa el mensaje que emite el sistema? 

R: Que no puede agregar esa tupla porque rompe la consistencia de la tabla ayud�ndose en la llave for�nea

- �Qu� significado tiene la sentencia anterior? 

R: Simplemente define la llave for�nea de la tabla asegurando que no haya inconsistencias

  sp_helpconstraint tableName 

- �Qu� significan las columnas de esas consultas?

R: Es la informaci�n de los constraints, como su nombre y el estado en el que se encuentra cada uno.

- �Qu� uso se le est� dando a GETDATE()? 

R: Se utiliza para que se capture la fecha actual.

- �Tiene sentido el valor del campo de cantidad? 

R: No porque no tiene sentido que se haga entrega de 0 cosas.

Intenta insertar un registro con cantidad igual o menor que 0. 

- �C�mo responde el sistema? 

R: No deja hacer el registro, marca conflicto con el constraint "cantidad"

- �Qu� significa el mensaje? 

R: Significa que no cumple con el constraint "cantidad" que se acaba de crear, porque se especific� que la cantidad ten�a que ser mayor a 0


Integridad referencial: La integridad referencial garantiza que la relaci�n etre dos tablas permanecer� sincronizada, ya sea cuando hay actualizaciones o alguna eliminaci�n.

FUENTE: https://msdn.microsoft.com/es-MX/library/aa292166(v=vs.71).aspx