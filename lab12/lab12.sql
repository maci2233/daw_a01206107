BULK INSERT a1206107.a1206107.[Materiales]
  FROM 'e:\wwwroot\a1206107\materiales.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

BULK INSERT a1206107.a1206107.[Proyectos]
  FROM 'e:\wwwroot\a1206107\Proyectos.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

BULK INSERT a1206107.a1206107.[Proveedores]
  FROM 'e:\wwwroot\a1206107\Proveedores.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

SET DATEFORMAT dmy -- especificar formato de la fecha

BULK INSERT a1206107.a1206107.[Entregan]
  FROM 'e:\wwwroot\a1206107\Entregan.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC)
ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY (Numero)
ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave,RFC,Numero,Fecha)

SELECT *
From Entregan

 ALTER TABLE entregan add constraint cfentreganclave
  foreign key (clave) references materiales(clave)

 ALTER TABLE entregan add constraint cfentreganRFC
  foreign key (rfc) references Proveedores(RFC);

 ALTER TABLE entregan add constraint cfentreganNumero
  foreign key (numero) references Proyectos(Numero);

  INSERT INTO entregan values (0, 'xxx', 0, '1-jan-02', 0) ;

 sp_helpconstraint Entregan

 INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0);

delete from Entregan where cantidad=0;

ALTER TABLE entregan add constraint cantidad check (cantidad > 0) ;
