<?php

	function conecta(){
		$mysql = mysqli_connect("localhost","root","","talleres");
 		$mysql->set_charset("utf8");
 		return $mysql;
	}

	function desconecta($mysql){
		mysqli_close($mysql);
	}

	function selectVoluntarios(){
		$db = conecta();
		$query = 'SELECT * FROM voluntario';
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function selectITESM(){
		$db = conecta();
		$query = 'SELECT * FROM voluntario WHERE escuela = "ITESM"';
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function selectErick(){
		$db = conecta();
		$query = 'SELECT * FROM voluntario WHERE nombre LIKE "%Erick%"';
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function showVoluntarios(){
		$result = selectVoluntarios();
		echo "<h4>Lista de voluntarios</h4><br><br>";
		echo "<table class=bordered>";
		echo "<tr>";
		echo "<th>Nombre</th>";
		echo "<th>Telefono</th>";
		echo "<th>Matricula</th>";
		echo "<th>Escuela</th>";
		echo "<th>Editar</th><th>Eliminar</th>";
		echo "</tr>";
		while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
			echo "<tr>";
			echo "<td>" . $fila["nombre"] . "</td>";
			echo "<td>" . $fila["telefono"] . "</td>";
			echo "<td>" . $fila["matricula"] . "</td>";
			echo "<td>" . $fila["escuela"] . "</td>";
			echo "<td> <a class=btn href=editar.php?matricula=$fila[matricula] btn-floating btn-large cyan pulse><i class=material-icons>edit</i></a> </td>";
			echo "<td> <a class=btn href=eliminar.php?matricula=$fila[matricula] btn-floating btn-large cyan pulse><i class=material-icons>delete_forever</i></a> </td>";
			echo "</tr>";
		}
		echo "</table>";
	}

	function showITESM(){
		$result = selectITESM();
		echo "<br><br><h4>Lista de voluntarios que estudian en el ITESM</h4><br><br>";
		echo "<table class=bordered>";
		echo "<tr>";
		echo "<th>Nombre</th>";
		echo "<th>Telefono</th>";
		echo "<th>Matricula</th>";
		echo "<th>Escuela</th>";
		echo "</tr>";
		while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
			echo "<tr>";
			echo "<td>" . $fila["nombre"] . "</td>";
			echo "<td>" . $fila["telefono"] . "</td>";
			echo "<td>" . $fila["matricula"] . "</td>";
			echo "<td>" . $fila["escuela"] . "</td>";
			echo "</tr>";
		}
		echo "</table>";

	}

	function showErick(){
		$result = selectErick();
		echo "<br><br><h4>Lista de voluntarios que se llaman Erick</h4>	<br><br>";
		echo "<table class=bordered>";
		echo "<tr>";
		echo "<th>Nombre</th>";
		echo "<th>Telefono</th>";
		echo "<th>Matricula</th>";
		echo "<th>Escuela</th>";
		echo "</tr>";
		while ($fila = mysqli_fetch_array($result,MYSQLI_BOTH)) {
			echo "<tr>";
			echo "<td>" . $fila["nombre"] . "</td>";
			echo "<td>" . $fila["telefono"] . "</td>";
			echo "<td>" . $fila["matricula"] . "</td>";
			echo "<td>" . $fila["escuela"] . "</td>";
			echo "</tr>";
		}
		echo "</table>";
	}

	function getRegistro($db,$matricula){
		$query = "SELECT * FROM voluntario WHERE matricula = $matricula";
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		return $fila;
		}
	

	function guardarRegistro($nombre,$telefono,$matricula,$escuela){	   
		$db = conecta();

		    $query='INSERT INTO voluntario (nombre,telefono,matricula,escuela) VALUES (?,?,?,?) ';

	    if (!($statement = $db->prepare($query))) {
	    	die("Preparation failed: (" . $db->errno . ") " . $db->error);
	    }
	    // Binding statement params 
	    if (!$statement->bind_param("ssis", $nombre, $telefono, $matricula, $escuela)) {
	        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    }
	    // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	     } 

		desconecta($db);
	}

	function editarRegistro($nombre,$telefono,$mat,$escuela){
		$db = conecta();

		 $query='UPDATE voluntario SET nombre=?, telefono=?, escuela=? WHERE matricula=?';

	    if (!($statement = $db->prepare($query))) {
	    	die("Preparation failed: (" . $db->errno . ") " . $db->error);
	    }
	    // Binding statement params 
	    if (!$statement->bind_param("sssi", $nombre, $telefono, $escuela, $mat)) {
	        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    }
	    // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	     } 
	     
		desconecta($db);	
	}

	function eliminarRegistro($matricula){
		$db = conecta();
		$query = "DELETE FROM voluntario WHERE matricula = $matricula";
		if(mysqli_query($db,$query)){
			echo "eliminado";
		}
		else{
			echo "nel";
		}
	}
?>