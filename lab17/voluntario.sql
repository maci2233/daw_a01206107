-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-10-2017 a las 16:41:17
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `talleres`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `voluntario`
--

CREATE TABLE `voluntario` (
  `nombre` varchar(50) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `matricula` int(5) NOT NULL,
  `escuela` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `voluntario`
--

INSERT INTO `voluntario` (`nombre`, `telefono`, `matricula`, `escuela`) VALUES
('Juan Ponce', '4611207543', 9, 'Marista'),
('Alan Macinco', '461111111', 17, 'ITAM'),
('Antonio López', '4461312312', 30, 'Santa Rosa'),
('Daniel Méndez', '461123321', 33, 'Rosen'),
('Raquel Martínez', '442123542', 100, 'Tecno'),
('Erwin Julián', '4420674512', 101, 'Oxford'),
('Alan Macías ', '4611467418', 142, 'ITESM'),
('Rubisela Macías', '432567971', 143, 'Pereyra');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `voluntario`
--
ALTER TABLE `voluntario`
  ADD PRIMARY KEY (`matricula`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
