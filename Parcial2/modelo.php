<?php
	
		function conecta(){
		$mysql = mysqli_connect("localhost","root","","zombies");
 		$mysql->set_charset("utf8");
 		return $mysql;
	}

	function desconecta($mysql){
		mysqli_close($mysql);
	}

	function selectZombies(){
		$db=conecta();
		$query = 'SELECT * FROM zombie ORDER BY nombre';
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function showZombies(){
		$result = selectZombies();
		$zombies = ['Infección','Coma','Transformación','Completamente Muerto'];
		echo "<table class=\"bordered\">";
		echo "<tr>";
		echo "<th>Nombre Completo</th>";
		echo "<th>Estado</th>";
		echo "<th>Fecha de Registro</th>";
		echo "<th>Fecha de modificación</th>";
		echo "<th>Editar</th>";
		echo "</tr>";
		while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
		echo "<tr>";
		echo "<td>" . $fila["nombre"] . "</td>";
		echo "<td>" . $zombies[$fila["estado"]] . "</td>";
		echo "<td>" . $fila["fechaC"] . "</td>";
		echo "<td>" . $fila["fechaM"] . "</td>";
		echo "<td><a class=\"btn-floating btn-large waves-effect waves-light green\" href=\"editarZombie.php?fecha=$fila[fechaC]\"><i class=\"material-icons\">edit</i></a></td>";
		echo "</tr>";
		}
		echo "</table>";
	}


	function selectOrdenar(){
		$db=conecta();
		$query = 'SELECT * FROM zombie ORDER BY fechaC';
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function showOrdenar(){
		$result = selectOrdenar();
		$zombies = ['Infección','Coma','Transformación','Completamente Muerto'];
		echo "<table class=\"bordered\">";
		echo "<tr>";
		echo "<th>Nombre Completo</th>";
		echo "<th>Estado</th>";
		echo "<th>Fecha de Registro</th>";
		echo "<th>Fecha de modificación</th>";
		echo "<th>Editar</th>";
		echo "</tr>";
		while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
		echo "<tr>";
		echo "<td>" . $fila["nombre"] . "</td>";
		echo "<td>" . $zombies[$fila["estado"]] . "</td>";
		echo "<td>" . $fila["fechaC"] . "</td>";
		echo "<td>" . $fila["fechaM"] . "</td>";
		echo "<td><a class=\"btn-floating btn-large waves-effect waves-light green\" href=\"editarZombie.php?fecha=$fila[fechaC]\"><i class=\"material-icons\">edit</i></a></td>";
		echo "</tr>";
		}
		echo "</table>";
	}

	function selectEstado($estado){
		$db=conecta();
		$query = "SELECT * FROM zombie WHERE estado = $estado";
		$result = mysqli_query($db,$query);
		desconecta($db);
		return $result;
	}

	function showEstado($estado){
		if($estado!=-1){
		$result = selectEstado($estado);
		$zombies = ['Infección','Coma','Transformación','Completamente Muerto'];
		echo "<table class=\"bordered\">";
		echo "<tr>";
		echo "<th>Nombre Completo</th>";
		echo "<th>Estado</th>";
		echo "<th>Fecha de Registro</th>";
		echo "<th>Fecha de modificación</th>";
		echo "<th>Editar</th>";
		echo "</tr>";
		while($fila = mysqli_fetch_array($result,MYSQLI_BOTH)){
		echo "<tr>";
		echo "<td>" . $fila["nombre"] . "</td>";
		echo "<td>" . $zombies[$fila["estado"]] . "</td>";
		echo "<td>" . $fila["fechaC"] . "</td>";
		echo "<td>" . $fila["fechaM"] . "</td>";
		echo "<td><a class=\"btn-floating btn-large waves-effect waves-light green\" href=\"editarZombie.php?fecha=$fila[fechaC]\"><i class=\"material-icons\">edit</i></a></td>";
		echo "</tr>";
		}
		echo "</table>";
		}
	}


	function agregarZombie($nombre,$estado){
		$db = conecta();

		$query='INSERT INTO zombie (nombre,estado) VALUES (?,?)';

	    if (!($statement = $db->prepare($query))) {
	    	die("Preparation failed: (" . $db->errno . ") " . $db->error);
	    }
	    // Binding statement http_parse_params(	

	    if (!$statement->bind_param("si", $nombre, $estado)) {
	        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    }
	    // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	     }
		desconecta($db);
	}

		function getZombie($db,$fecha){
		$query = "SELECT * FROM zombie WHERE fechaC LIKE '$fecha'";
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		return $fila;
		}

	function editarZombie($nombre,$estado,$fecha){
		$db = conecta();
		$modif = time();

		$query='UPDATE zombie SET nombre=?,estado=?,fechaM=? WHERE fechaC LIKE ? ';

	    if (!($statement = $db->prepare($query))) {
	    	die("Preparation failed: (" . $db->errno . ") " . $db->error);
	    }
	    // Binding statement http_parse_params(	

	    if (!$statement->bind_param("siss", $nombre, $estado, $modif, $fecha)) {
	        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
	    }
	    // Executing the statement
	    if (!$statement->execute()) {
	        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
	     } 

		desconecta($db);
	}


	function totalZombies(){
		$db=conecta();
		$query = '	SELECT count(*) 
					FROM zombie';
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		echo "<h5>En total hay " . $fila[0] . " zombies registrados</h5>";
		desconecta($db);
	}

	function totalEstado(){
		$zombies = ['Infección','Coma','Transformación','Completamente Muerto'];
		$db=conecta();

		//INFECCION

		$query = '	SELECT count(*) 
					FROM zombie
					WHERE estado = 0';
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		echo "<br><h5>En total hay " . $fila[0] . " zombie(s) en el estado: Infección</h5><br>";

		//COMA

		$query = '	SELECT count(*) 
			FROM zombie
			WHERE estado = 1';
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		echo "<h5>En total hay " . $fila[0] . " zombie(s) en el estado: Coma</h5><br>";

		//TRANSFORMACIÓN

		$query = '	SELECT count(*) 
			FROM zombie
			WHERE estado = 2';
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		echo "<h5>En total hay " . $fila[0] . " zombie(s) en el estado: Transformaciónn</h5><br>";

		//COMPLETAMENTE MUERTO

		$query = '	SELECT count(*) 
			FROM zombie
			WHERE estado = 3';
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		echo "<h5>En total hay " . $fila[0] . " zombie(s) en el estado: Completamente Muerto</h5><br>";

		//NO COPLETAMENTE MUERTOS

		$query = 'SELECT count(*) 
				FROM zombie
				WHERE estado=0 OR estado=1 OR estado = 2';
		$result = mysqli_query($db,$query);
		$fila = mysqli_fetch_array($result,MYSQLI_BOTH);
		echo "<h5>En total hay " . $fila[0] . " zombie(s) que no están en el estado: Completamente Muerto</h5>";
		desconecta($db);	
	}



?>