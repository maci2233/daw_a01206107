-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2017 a las 22:59:28
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `zombies`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zombie`
--

CREATE TABLE `zombie` (
  `nombre` varchar(100) NOT NULL,
  `estado` int(1) NOT NULL,
  `fechaC` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaM` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `zombie`
--

INSERT INTO `zombie` (`nombre`, `estado`, `fechaC`, `fechaM`) VALUES
('Erwin Julián', 2, '2017-10-27 19:00:11', '2017-10-27 19:00:11'),
('Danielsillo', 3, '2017-10-27 19:12:58', '2017-10-27 19:12:58'),
('Hockey', 0, '2017-10-27 19:13:08', '2017-10-27 19:13:08'),
('Alan Antonio', 0, '2017-10-27 19:14:05', '2017-10-27 19:14:05'),
('macinco', 2, '2017-10-27 19:18:08', '2017-10-27 19:18:08'),
('MACIAS', 1, '2017-10-27 19:38:03', '2017-10-27 19:38:03'),
('MACINDER', 1, '2017-10-27 19:43:49', '2017-10-27 19:43:49'),
('Esteban', 3, '2017-10-27 20:39:34', '2017-10-27 20:39:34'),
('Sebastián', 1, '2017-10-27 20:57:21', '2017-10-27 20:57:21'),
('Marco', 3, '2017-10-27 20:57:28', '2017-10-27 20:57:28');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `zombie`
--
ALTER TABLE `zombie`
  ADD PRIMARY KEY (`fechaC`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
