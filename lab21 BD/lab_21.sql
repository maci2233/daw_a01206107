/* TABLA MATERIALES*/

/* CREA MATERIAL*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaMaterial' AND type = 'P')
    DROP PROCEDURE creaMaterial
GO

CREATE PROCEDURE creaMaterial
    @uclave NUMERIC(5,0),
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2),
    @uimpuesto NUMERIC(6,2)
AS
    INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
GO

EXECUTE creaMaterial 5000,'Martillos Acme',250,15

/*MODIFICA MATERIAL*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'modificaMaterial' AND type = 'P')
    DROP PROCEDURE modificaMaterial
GO

CREATE PROCEDURE modificaMaterial
    @uclave NUMERIC(5,0),
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2),
    @uimpuesto NUMERIC(6,2)
AS
    UPDATE Materiales SET Descripcion=@udescripcion, Costo=@ucosto, PorcentajeImpuesto=@uimpuesto WHERE clave=@uclave
GO

EXECUTE modificaMaterial 5000,'Martillos Acme',300,20

/*ELIMINA MATERIAL*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'eliminaMaterial' AND type = 'P')
    DROP PROCEDURE eliminaMaterial
GO

CREATE PROCEDURE eliminaMaterial
    @uclave NUMERIC(5,0)
AS
    DELETE FROM Materiales WHERE clave=@uclave
GO

EXECUTE eliminaMaterial 5000

SELECT *
from Materiales
where clave = 5000

/*TABLA PROYECTOS*/

/*CREA PROYECTO*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaProyecto' AND type = 'P')
    DROP PROCEDURE creaProyecto
GO

CREATE PROCEDURE creaProyecto
    @uNumero NUMERIC(5,0),
    @uDenominacion VARCHAR(50)

AS
    INSERT INTO Proyectos VALUES(@uNumero, @uDenominacion)
GO

EXECUTE creaProyecto 5020,'Donación a Macías'

/*MODIFICA PROYECTO*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'modificaProyecto' AND type = 'P')
    DROP PROCEDURE modificaProyecto
GO

CREATE PROCEDURE modificaProyecto
    @uNumero NUMERIC(5,0),
    @uDenominacion VARCHAR(50)
AS
    UPDATE Proyectos SET Denominacion=@uDenominacion WHERE Numero=@uNumero
GO

EXECUTE modificaProyecto 5020,'Donación a Macinder'

/*ELIMINA PROYECTO*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'eliminaProyecto' AND type = 'P')
    DROP PROCEDURE eliminaProyecto
GO

CREATE PROCEDURE eliminaProyecto
    @uNumero NUMERIC(5,0)
AS
    DELETE FROM Proyectos WHERE Numero=@uNumero
GO

EXECUTE eliminaProyecto 5020

SELECT *
FROM Proyectos
WHERE Numero=5020

/*TABLA PROVEEDORES*/

/*CREA PROVEEDOR*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaProveedor' AND type = 'P')
    DROP PROCEDURE creaProveedor
GO

CREATE PROCEDURE creaProveedor
    @uRFC CHAR(13),
    @uRazonSocial VARCHAR(50)
AS
    INSERT INTO Proveedores VALUES(@uRFC, @uRazonSocial)
GO

EXECUTE creaProveedor 'a123','MACINDER'

/*MODIFICA PROVEEDOR*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'modificaProveedor' AND type = 'P')
    DROP PROCEDURE modificaProveedor
GO

CREATE PROCEDURE modificaProveedor
    @uRFC CHAR(13),
    @uRazonSocial VARCHAR(50)
AS
    UPDATE Proveedores SET RazonSocial=@uRazonSocial WHERE RFC=@uRFC
GO

EXECUTE modificaProveedor 'a123','Macias'

/*ELIMINA PROVEEDOR*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'eliminaProveedor' AND type = 'P')
    DROP PROCEDURE eliminaProveedor
GO

CREATE PROCEDURE eliminaProveedor
    @uRFC CHAR(13)
AS
    DELETE FROM Proveedores WHERE RFC=@uRFC
GO

EXECUTE eliminaProveedor 'a123'

SELECT *
FROM Proveedores
WHERE RFC = 'a123'

/*TABLA ENTREGAN*/

/*CREA ENTREGAN*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaEntregan' AND type = 'P')
    DROP PROCEDURE creaEntregan
GO

CREATE PROCEDURE creaEntregan
    @uclave NUMERIC(5,0),
    @uRFC CHAR(13),
    @uNumero NUMERIC(5,0),
    @uFecha DATETIME,
    @uCantidad NUMERIC(8,2)
AS
    INSERT INTO Entregan VALUES(@uclave, @uRFC, @uNumero, @uFecha,@uCantidad)
GO

set DATEFORMAT dmy

EXECUTE creaEntregan 5000,'a123',5020,'01-01-2000',125

/*MODIFICA ENTREGAN*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'modificaEntregan' AND type = 'P')
    DROP PROCEDURE modificaEntregan
GO

CREATE PROCEDURE modificaEntregan
    @uclave NUMERIC(5,0),
    @uRFC CHAR(13),
    @uNumero NUMERIC(5,0),
    @uFecha DATETIME,
    @uFechaNew DATETIME,
    @uCantidad NUMERIC(8,2)
AS
    UPDATE Entregan SET Fecha=@uFechaNew, Cantidad=@uCantidad WHERE Clave=@uclave AND RFC=@uRFC AND Numero=@uNumero AND Fecha=@uFecha
GO

EXECUTE modificaEntregan 5000,'a123',5020,'01-01-2000','01-01-2010',150

/*ELIMINA ENTREGAN*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'eliminaEntregan' AND type = 'P')
    DROP PROCEDURE eliminaEntregan
GO

CREATE PROCEDURE eliminaEntregan
    @uclave NUMERIC(5,0),
    @uRFC CHAR(13),
    @uNumero NUMERIC(5,0),
    @uFecha DATETIME
AS
    DELETE FROM Entregan WHERE Clave=@uclave AND RFC=@uRFC AND Numero=@uNumero AND Fecha=@uFecha
GO

EXECUTE eliminaEntregan 5000,'a123',5020,'01-01-2010'

SELECT *
FROM Entregan
WHERE clave=5000 AND RFC='a123' AND Numero=5020 AND fecha='01-01-2010'

/*HACIENDO QUERY*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'queryMaterial' AND type = 'P')
    DROP PROCEDURE queryMaterial
GO

CREATE PROCEDURE queryMaterial
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2)

AS
    SELECT * FROM Materiales WHERE descripcion
    LIKE '%'+@udescripcion+'%' AND costo > @ucosto
GO

EXECUTE queryMaterial 'Lad',20

/*STORE PROCEDURE PARA PROYECTO DE LA FUNDACIÓN EL ARCA*/

/*AGREGANDO REGISTRO A LA TABLA VOLUNTARIO*/

IF EXISTS (SELECT name FROM sysobjects
           WHERE name = 'creaVoluntario' AND type = 'P')
    DROP PROCEDURE creaVoluntario
GO

CREATE PROCEDURE creaVoluntario
    @unombre VARCHAR(50),
    @uapellidos VARCHAR(50),
    @ulugar VARCHAR(100),
    @ufecha DATETIME,
    @ucorreo VARCHAR(70),
    @utelefono VARCHAR(15),
    @uciudad VARCHAR(30),
    @upais VARCHAR(30),
    @uinteres NUMERIC(1)
AS
    INSERT INTO Voluntario VALUES(@unombre, @uapellidos,@ulugar,@ufecha,@ucorreo,@utelefono,@uciudad,@upais,@uinteres)
GO