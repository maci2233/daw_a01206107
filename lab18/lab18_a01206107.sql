set DATEFORMAT ymd

SELECT costo,cantidad,sum(Cantidad) as cantidadTotal, ((costo + costo*(PorcentajeImpuesto/100))*cantidad) as importe
FROM Entregan E, Materiales M
WHERE M.Clave = E.Clave AND fecha BETWEEN '1997-01-01' AND '1997/12/31'
GROUP BY costo,Cantidad,PorcentajeImpuesto


SELECT RazonSocial, count(*) as numEntregas, sum(cantidad*( costo + costo*(PorcentajeImpuesto/100) )) as importeTotal
FROM entregan e,Materiales m,Proveedores p
WHERE e.Clave = m.Clave AND p.RFC = e.RFC
GROUP BY RazonSocial

SELECT m.clave,Descripcion,sum(Cantidad) as cantidadTotal,min(cantidad) as cantidadMin, max(Cantidad) as cantidadMax, avg(cantidad) as cantidadProm, sum(cantidad*costo + costo*(PorcentajeImpuesto/100 )) as importeTotal
FROM Materiales m, Entregan e
WHERE m.Clave = e.Clave
GROUP BY m.Clave,Descripcion
HAVING avg(cantidad)>400

SELECT RazonSocial,Descripcion, e.clave, avg(Cantidad) as cantidadProm
FROM Proveedores p, Entregan e, Materiales m
WHERE p.RFC = e.RFC AND e.Clave=m.Clave
GROUP BY RazonSocial,Descripcion,e.Clave
HAVING avg(cantidad)<500
ORDER BY RazonSocial

SELECT RazonSocial,Descripcion, e.clave, avg(Cantidad) as cantidadProm
FROM Proveedores p, Entregan e, Materiales m
WHERE p.RFC = e.RFC AND e.Clave=m.Clave
GROUP BY RazonSocial,Descripcion,e.Clave
HAVING avg(cantidad)<370 OR avg(cantidad)>450
ORDER BY RazonSocial,cantidadProm

INSERT INTO Materiales VALUES (1440,'Madera Fresno', 260,2.70)
INSERT INTO Materiales VALUES (1450,'Cobre amarillo', 260,2.70)
INSERT INTO Materiales VALUES (1460,'Tabla Nogal', 100,2.50)
INSERT INTO Materiales VALUES (1470,'Tornillo 3.5', 100,2.50)
INSERT INTO Materiales VALUES (1480,'Motor 4.2', 200,2.48)

SELECT Descripcion,Clave
FROM Materiales
WHERE clave NOT IN ( SELECT e.clave
                      FROM Materiales m,Entregan e
                      WHERE m.Clave = e.Clave)

SELECT DISTINCT RazonSocial
FROM Proveedores p, Entregan e,Proyectos pro
WHERE p.RFC = e.RFC AND pro.Numero = e.Numero AND Denominacion LIKE '%Vamos Mexico%'
      AND RazonSocial IN (SELECT DISTINCT RazonSocial
                          FROM Proveedores p, Entregan e,Proyectos pro
                          WHERE p.RFC = e.RFC AND pro.Numero = e.Numero AND Denominacion LIKE '%Queretaro Limpio%')


SELECT Descripcion,Clave
FROM Materiales
WHERE clave NOT IN ( SELECT e.clave
                      FROM Materiales m,Entregan e,Proyectos p
                      WHERE m.Clave = e.Clave AND Denominacion LIKE '%CIT Yucatan%')

SELECT RazonSocial,avg(Cantidad) as promCantidad
FROM Proveedores p,Entregan e
WHERE p.RFC = e.RFC
GROUP BY RazonSocial
HAVING avg(cantidad) > (SELECT avg(Cantidad)
                        FROM Proveedores p,Entregan e
                        WHERE p.RFC = e.RFC AND p.RFC LIKE '%VAGO780901%')

SELECT RFC
FROM Proveedores
/*NINGUN PROVEEDOR TIENE ESE RFC*/


CREATE VIEW info2001 AS
(
SELECT p.RazonSocial,p.RFC,sum(Cantidad) as cantidades
FROM Proveedores p, Entregan e,Proyectos pro
WHERE p.RFC = e.RFC AND pro.Numero = e.Numero AND Denominacion LIKE '%Infonavit Durango%' AND Fecha BETWEEN '2001-01-01' AND '2001-12-31'
GROUP BY p.RazonSocial,p.RFC
)

CREATE VIEW info2000 AS
(
SELECT p.RazonSocial,p.RFC,sum(Cantidad) as cantidades
FROM Proveedores p, Entregan e,Proyectos pro
WHERE p.RFC = e.RFC AND pro.Numero = e.Numero AND Denominacion LIKE '%Infonavit Durango%' AND Fecha BETWEEN '2000-01-01' AND '2000-12-31'
GROUP BY p.RazonSocial,p.RFC
)

SELECT info2000.RFC,info2000.RazonSocial,info2000.cantidades
FROM info2000 , info2001
WHERE info2000.RFC = info2001.RFC AND info2000.RazonSocial = info2001.RazonSocial AND info2000.cantidades > info2001.cantidades
